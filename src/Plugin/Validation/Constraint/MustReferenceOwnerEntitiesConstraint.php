<?php declare(strict_types=1);

namespace Drupal\limit_entity_reference_owner\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides a MustReferenceOwnerEntities constraint.
 *
 * @Constraint(
 *   id = "MustReferenceOwnerEntities",
 *   label = @Translation("MustReferenceOwnerEntities", context = "Validation"),
 * )
 *
 * @DCG
 * To apply this constraint on third party entity types implement either
 * hook_entity_base_field_info_alter() or hook_entity_bundle_field_info_alter().
 *
 * @see https://www.drupal.org/node/2015723
 */
final class MustReferenceOwnerEntitiesConstraint extends Constraint {

  public string $message = 'The %field_label field cannot reference entities that are owned by other users.';

}
