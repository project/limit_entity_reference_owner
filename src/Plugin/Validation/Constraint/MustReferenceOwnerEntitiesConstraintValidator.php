<?php declare(strict_types=1);

namespace Drupal\limit_entity_reference_owner\Plugin\Validation\Constraint;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\user\EntityOwnerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the ReferenceOwnEntities constraint.
 */
final class MustReferenceOwnerEntitiesConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate(mixed $items, Constraint $constraint): void {
    if (!$items instanceof FieldItemListInterface) {
      throw new \InvalidArgumentException(
        sprintf('The validated value must be instance of \Drupal\Core\Field\FieldItemListInterface, %s was given.', get_debug_type($items))
      );
    }

    foreach ($items as $delta => $item) {
      if (!($item instanceof EntityReferenceItem)) {
        continue;
      }

      // If there is a media reference field on a node, $item->getEntity()
      // returns the node and $item->entity returns the media.
      $parent_entity = $item->getEntity();
      $referenced_entity = $item->entity;

      if (!($referenced_entity instanceof EntityOwnerInterface)) {
        continue;
      }

      $validate_user_id = $parent_entity instanceof EntityOwnerInterface && $parent_entity->getOwnerId()
        ? $parent_entity->getOwnerId()
        : \Drupal::currentUser()->id();
      if ($validate_user_id === $referenced_entity->getOwnerId()) {
        continue;
      }

      $field_label = $item->getFieldDefinition()->getLabel();

      $this->context->buildViolation($constraint->message, ['%field_label' => $field_label])
        ->atPath($delta)
        ->addViolation();
    }
  }

}
